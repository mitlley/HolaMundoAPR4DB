package inacap.test.holamundoapr4db;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import inacap.test.holamundoapr4db.controlador.UsuariosController;
import inacap.test.holamundoapr4db.modelo.sqlite.HolaMundoDBContract;
import inacap.test.holamundoapr4db.vista.FormularioActivity;
import inacap.test.holamundoapr4db.vista.HomeActivity;
import inacap.test.holamundoapr4db.vista.HomeDrawerActivity;

public class MainActivity extends AppCompatActivity {

    private EditText editTextUsername, etPassword;
    private Button btLogin;
    private TextView tvPassword, tvRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Consultamos si la persona ya inicio sesion
        // Abrimos el archivo de preferencias compartidas
        SharedPreferences sesiones = getSharedPreferences(HolaMundoDBContract.HolaMundoSesiones.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        boolean yaInicioSesion = sesiones.getBoolean(HolaMundoDBContract.HolaMundoSesiones.FIELD_SESION, false);
        if(yaInicioSesion){
            Intent i = new Intent(MainActivity.this, HomeDrawerActivity.class);
            startActivity(i);
            finish();
        }

        this.editTextUsername = (EditText) findViewById(R.id.etUsername);
        this.etPassword = (EditText) findViewById(R.id.etPassword);

        this.btLogin = (Button) findViewById(R.id.btLogin);
        this.tvPassword = (TextView) findViewById(R.id.tvPassword);
        this.tvRegister = (TextView) findViewById(R.id.tvRegister);

        // Esperar a que el usuario haga 'click' en el Button
        this.btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtener datos

                String username = editTextUsername.getText().toString();
                String password = etPassword.getText().toString();

                UsuariosController controller = new UsuariosController(getApplicationContext());
                boolean login = controller.usuarioLogin(username, password);

                if(login){

                    // Guardar un pequeño dato que nos diga que la persona
                    // inicio sesion

                    SharedPreferences sesiones = getSharedPreferences(HolaMundoDBContract.HolaMundoSesiones.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sesiones.edit();

                    // Guardamos el primer dato
                    editor.putBoolean(HolaMundoDBContract.HolaMundoSesiones.FIELD_SESION, true);
                    editor.commit();

                    Intent i = new Intent(MainActivity.this, HomeDrawerActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Datos incorrectos", Toast.LENGTH_SHORT).show();
                }

            }
        });

        this.tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // INiciar la segunda activity
                Intent nuevaVentana = new Intent(MainActivity.this, FormularioActivity.class);
                startActivity(nuevaVentana);

            }
        });

    }
}











