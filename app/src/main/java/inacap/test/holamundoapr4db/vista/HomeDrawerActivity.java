package inacap.test.holamundoapr4db.vista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import inacap.test.holamundoapr4db.MainActivity;
import inacap.test.holamundoapr4db.R;
import inacap.test.holamundoapr4db.modelo.sqlite.HolaMundoDBContract;
import inacap.test.holamundoapr4db.vista.fragmentos.BienvenidaFragment;
import inacap.test.holamundoapr4db.vista.fragmentos.MapaFragment;

public class HomeDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BienvenidaFragment.OnFragmentInteractionListener, MapaFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_drawer);

        // Llamamos al componente ToolBar (Barra superior o barra de herramientas)
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Boton flotante
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Mensaje temporal numero 2 (1 = Toast)
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Layout que permite la existencia del panel lateral
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // Panel lateral
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        // Ponemos el panel lateral en modo de escucha
        // Cuando se selecciona un item del menu del panel lateral
        // se ejecuta el metodo OnNavigationItemSelected
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        // Sobreescribir el comportamiento del boton 'atras'
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Si el panel lateral esta abierto
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            // Cerramos el panel lateral
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // De lo contrario, actuamos de manera normal
            //Toast.makeText(getApplicationContext(), "Adios!", Toast.LENGTH_SHORT).show();
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_logout){
            SharedPreferences preferences = getSharedPreferences(HolaMundoDBContract.HolaMundoSesiones.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();

            editor.putBoolean(HolaMundoDBContract.HolaMundoSesiones.FIELD_SESION, false);
            editor.putString(HolaMundoDBContract.HolaMundoSesiones.FIELD_USERNAME, "");
            editor.commit();

            Intent i  = new Intent(HomeDrawerActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Este metodo reacciona cuando la persona selecciona
        // un item del menu del panel lateral

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragmento = null;

        if (id == R.id.nav_camera) {
            // Handle the camera action
            fragmento = new BienvenidaFragment();
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else if (id == R.id.nav_hola){
            fragmento = new MapaFragment();
            Toast.makeText(getApplicationContext(), "Bienvenido!!", Toast.LENGTH_SHORT).show();
        }

        if(fragmento != null){
            // Mostrar el fragmento en la activity
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.flContent, fragmento).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(String fragmentName, String event) {

        if(fragmentName.equals("BienvenidaFragment")){
            if(event.equals("CONTADOR_AUMENTA")){
                Toast.makeText(getApplicationContext(), "Contador aumento", Toast.LENGTH_SHORT).show();
            }
        }

    }
}
