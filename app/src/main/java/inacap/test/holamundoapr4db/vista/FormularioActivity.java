package inacap.test.holamundoapr4db.vista;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import inacap.test.holamundoapr4db.R;
import inacap.test.holamundoapr4db.controlador.UsuariosController;
import inacap.test.holamundoapr4db.modelo.sqlite.HolaMundoDBContract;
import inacap.test.holamundoapr4db.modelo.sqlite.UsuariosModel;

/**
 * Created by mitlley on 25-08-17.
 */

public class FormularioActivity extends AppCompatActivity {

    private EditText etUsername, etPassword1, etPassword2;
    private Button btRegister;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        this.etUsername = (EditText) findViewById(R.id.etUsername);
        this.etPassword1 = (EditText) findViewById(R.id.etPassword1);
        this.etPassword2 = (EditText) findViewById(R.id.etPassword2);

        this.btRegister = (Button) findViewById(R.id.btRegister);

        this.btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Capturar datos ingresados por la persona
                String username = etUsername.getText().toString();
                String password = etPassword1.getText().toString();
                String password2 = etPassword2.getText().toString();

                try {
                    UsuariosController controller = new UsuariosController(getApplicationContext());
                    controller.crearUsuario(username, password, password2);
                } catch(Exception e ){
                    String mensaje = e.getMessage();
                    Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}





