package inacap.test.holamundoapr4db.modelo.sqlite;

import android.provider.BaseColumns;

/**
 * Created by mitlley on 28-08-17.
 */

public class HolaMundoDBContract {
    private HolaMundoDBContract(){}

    public static class HolaMundoDBUsuarios implements BaseColumns {
        public static final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_PASSWORD = "password";
    }

    public static class HolaMundoSesiones {
        // Nombre del archivo de las preferencias compartidas
        public static final String SHARED_PREFERENCES_NAME = "sesiones";
        // Nombre del primer campo o variable a almacenar
        public static final String FIELD_SESION = "sesion";

        public static final String FIELD_USERNAME = "username";
    }
}
