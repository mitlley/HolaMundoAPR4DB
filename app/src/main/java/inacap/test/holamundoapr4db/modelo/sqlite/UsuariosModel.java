package inacap.test.holamundoapr4db.modelo.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by mitlley on 01-09-17.
 */

public class UsuariosModel {
    private HolaMundoDBHelper dbHelper;

    public UsuariosModel(Context con){
        this.dbHelper = new HolaMundoDBHelper(con);
    }

    public void crearUsuario(ContentValues usuario){
        // Llamamos a la base de datos
        SQLiteDatabase base_datos = this.dbHelper.getWritableDatabase();
        base_datos.insert(HolaMundoDBContract.HolaMundoDBUsuarios.TABLE_NAME, null, usuario);
    }

    public ContentValues obtenerUsuarioPorUsername(String username){
        // Abrimos una conexion a la BD
        SQLiteDatabase base_datos = this.dbHelper.getReadableDatabase();

        // SELECT * FROM usuarios WHERE username = 'david' LIMIT 1

        // Seleccionar las columnas (projection)

        String[] projection = {
                HolaMundoDBContract.HolaMundoDBUsuarios._ID,
                HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_USERNAME,
                HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_PASSWORD
        };

        // Como filtraremos
        String selection = HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_USERNAME + " = ? LIMIT 1";

        // Argumentos de filtrado
        String[] selectionArgs = { username };

        // Hacemos la consulta

        Cursor cursor = base_datos.query(
                HolaMundoDBContract.HolaMundoDBUsuarios.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null, null, null
        );

        // Mover el cursor a la primera respuesta
        if(cursor.moveToFirst()){

            // Tomamos los datos del cursor
            String cursor_username = cursor.getString(cursor.getColumnIndex(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_USERNAME));
            String cursor_password = cursor.getString(cursor.getColumnIndex(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_PASSWORD));

            ContentValues usuario = new ContentValues();
            usuario.put(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_USERNAME, cursor_username);
            usuario.put(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_PASSWORD, cursor_password);

            return usuario;

        }

        return null;
    }

}
