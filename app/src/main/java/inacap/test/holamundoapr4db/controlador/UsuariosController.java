package inacap.test.holamundoapr4db.controlador;

import android.content.ContentValues;
import android.content.Context;

import inacap.test.holamundoapr4db.modelo.sqlite.HolaMundoDBContract;
import inacap.test.holamundoapr4db.modelo.sqlite.UsuariosModel;

/**
 * Created by mitlley on 04-09-17.
 */

public class UsuariosController {
    private UsuariosModel model;

    public UsuariosController(Context con){
        this.model = new UsuariosModel(con);
    }

    public void crearUsuario(String username, String password, String password2) throws Exception{

        //// VALIDACIONES
        if(!password.equals(password2)){
            // Lanzamos un Exception
            // DEBE SER MANEJADO POR LA CAPA VISTA
            throw new Exception("contraseñas no coinciden");
        }

        // Creamos un usuario en la base de datos a traves de la capa Modelo

        // Crear objeto con esos datos
        ContentValues usuario = new ContentValues();
        usuario.put(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_USERNAME, username);
        usuario.put(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_PASSWORD, password);

        //Guardar
        model.crearUsuario(usuario);
    }

    public boolean usuarioLogin(String username, String password){
        // Pedimos los datos del usuario a la BD
        ContentValues usuario = model.obtenerUsuarioPorUsername(username);

        // Comparamos la contraseña que ingreso la persona
        // con la que existe en la base de datos.

        if(password.equals(usuario.get(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_PASSWORD))){
            return true;
        }


        return false;
    }

}

















